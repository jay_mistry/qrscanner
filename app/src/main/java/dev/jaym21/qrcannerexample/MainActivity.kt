package dev.jaym21.qrcannerexample

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Size
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import dev.jaym21.qrcannerexample.databinding.ActivityMainBinding
import dev.jaym21.qrscanner.AadharCard
import dev.jaym21.qrscanner.QRAnalyzer
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding: ActivityMainBinding
        get() = _binding!!
    private var preview: Preview? = null
    private var camera: Camera? = null
    private var imageAnalysis: ImageAnalysis? = null
    private var cameraProvider: ProcessCameraProvider? = null
    private var aadharCard: AadharCard? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (checkCameraPermissions(this, arrayOf(android.Manifest.permission.CAMERA))) {
            initialize()
        } else {
            permissionRequestLauncher.launch(android.Manifest.permission.CAMERA)
        }
    }

    private fun initialize() {
        binding.viewFinder.post {
            setUpCamera()
        }
    }

    private fun setUpCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {
            // CameraProvider
            cameraProvider = cameraProviderFuture.get()

            // Build and bind the camera use cases
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(this))
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun bindCameraUseCases() {
        val previewSize =  Size(binding.viewFinder.width, binding.viewFinder.height)

        // Must unbind the use-cases before rebinding them
        cameraProvider?.unbindAll()

        preview = Preview.Builder()
            .setTargetResolution(previewSize)
            .build()

        imageAnalysis = ImageAnalysis.Builder()
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .setTargetResolution(previewSize)
            .build()

        imageAnalysis?.setAnalyzer(ContextCompat.getMainExecutor(this), { imageProxy ->
            QRAnalyzer.scanCode(imageProxy) {
                aadharCard = it
                Log.d("TAGYOYO", "bindCameraUseCases: $aadharCard")
                val intent = Intent(this, ResultActivity::class.java)
                intent.putExtra("aadharCard", Gson().toJson(aadharCard))
                startActivity(intent)
            }
        })

        try {
            camera = cameraProvider!!.bindToLifecycle(this, CameraSelector.DEFAULT_BACK_CAMERA, preview, imageAnalysis)

            // Attach the viewfinder's surface provider to preview use case
            preview?.setSurfaceProvider(binding.viewFinder.surfaceProvider)
        } catch (exc: Exception) {
            Log.e("TAGYOYO", "Use case binding failed", exc)
        }
    }

    private fun checkCameraPermissions(context: Context, permissions: Array<String>): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    private val permissionRequestLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
        if (isGranted) {
            initialize()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}