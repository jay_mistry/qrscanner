package dev.jaym21.qrcannerexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import dev.jaym21.qrcannerexample.databinding.ActivityResultBinding
import dev.jaym21.qrscanner.AadharCard

class ResultActivity : AppCompatActivity() {

    private lateinit var binding: ActivityResultBinding
    private var aadharCardString: String? = null
    private var aadharCard: AadharCard? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResultBinding.inflate(layoutInflater)
        setContentView(binding.root)

        aadharCardString =  intent.getStringExtra("aadharCard")

        if (!aadharCardString.isNullOrEmpty()) {
            aadharCard = Gson().fromJson(aadharCardString, AadharCard::class.java)

            if (aadharCard != null) {
                binding.tvResult.text =
                    "uid: ${aadharCard!!.uid} \n" +
                            "name: ${aadharCard!!.name} \n" +
                            "gender: ${aadharCard!!.gender} \n" +
                            "guardianName: ${aadharCard!!.guardianName} \n" +
                            "yearOfBirth: ${aadharCard!!.yearOfBirth} \n" +
                            "careOf: ${aadharCard!!.careOf} \n" +
                            "house: ${aadharCard!!.house} \n" +
                            "location: ${aadharCard!!.location} \n" +
                            "district: ${aadharCard!!.district} \n" +
                            "subDistrict: ${aadharCard!!.subDistrict} \n" +
                            "landmark: ${aadharCard!!.landmark} \n" +
                            "village: ${aadharCard!!.village} \n" +
                            "state: ${aadharCard!!.state} \n" +
                            "street: ${aadharCard!!.street} \n" +
                            "dateOfBirth: ${aadharCard!!.dateOfBirth} \n" +
                            "postOffice: ${aadharCard!!.postOffice} \n" +
                            "pinCode: ${aadharCard!!.pinCode}"
            }
        }
    }
}