package dev.jaym21.qrscanner

import android.annotation.SuppressLint
import android.util.Log
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.common.InputImage
import org.w3c.dom.Document
import org.w3c.dom.NamedNodeMap
import org.xml.sax.SAXException
import java.io.ByteArrayInputStream
import java.io.IOException
import java.text.ParseException
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.ParserConfigurationException

class QRAnalyzer {


    companion object {

        @SuppressLint("UnsafeOptInUsageError")
        fun scanCode(imageProxy: ImageProxy, onAadharDetected: (aadharCard: AadharCard) -> Unit) {
            var aadharCard: AadharCard? = null

            val inputImage = InputImage.fromMediaImage(imageProxy.image!!, imageProxy.imageInfo.rotationDegrees)

            val options = BarcodeScannerOptions.Builder()
                .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                .build()

            val scanner = BarcodeScanning.getClient(options)

            scanner.process(inputImage)
                .addOnSuccessListener { barcodes ->
                    barcodes.forEach {
                        aadharCard  = extractInformation(it.rawValue)
                        if (aadharCard != null)
                            onAadharDetected(aadharCard!!)
                    }
                }
                .addOnFailureListener { error->
                    imageProxy.close()
                }
                .addOnCompleteListener {
                    imageProxy.close()
                }
        }

        private fun extractInformation(rawValue: String?): AadharCard? {

            var scanResult: String? = rawValue
            var dom: Document? = null
            var aadharCard: AadharCard? = null

            if (scanResult != null) {

                val dbf = DocumentBuilderFactory.newInstance()
                try {

                    val db = dbf.newDocumentBuilder()

                    // Replace </?xml... with <?xml...
                    if (scanResult.startsWith("</?")) {
                        scanResult = scanResult.replaceFirst("</\\?", "<?")
                    }

                    // Replace <?xml...?"> with <?xml..."?>
                    scanResult = scanResult.replaceFirst("^<\\?xml ([^>]+)\\?\">", "<?xml $1\"?>")

                    //parse using builder to get DOM representation of the XML file
                    dom = db.parse(ByteArrayInputStream(scanResult.toByteArray(Charsets.UTF_8)))

                } catch (pce: ParserConfigurationException) {
                    dom = null
                    Log.d("TAGYOYO", "ParserConfigurationException: ${pce.localizedMessage}")
                } catch (saxe: SAXException) {
                    dom = null
                    Log.d("TAGYOYO", "SAXException: ${saxe.localizedMessage}")
                } catch (ioe: IOException) {
                    dom = null
                    Log.d("TAGYOYO", "IOException: ${ioe.localizedMessage}")
                }

                if (dom != null) {
                    val node = dom.childNodes.item(0)
                    val attributes = node.attributes
                    val uid = getAttribute(attributes, "uid")
                    val name = getAttribute(attributes, "name")
                    var rawGender = getAttribute(attributes, "gender")
                    try {
                        rawGender = formatGender(rawGender)
                    } catch (e: ParseException) {
                        System.err.println("Expected gender to be one of m, f, male, female; got $rawGender")
                    }
                    val gender = rawGender
                    val yob = getAttribute(attributes, "yob")
                    val gname = getAttribute(attributes, "gname")
                    val co = getAttribute(attributes, "co")
                    val house = getAttribute(attributes, "house")
                    val street = getAttribute(attributes, "street")
                    val lm = getAttribute(attributes, "lm")
                    val loc = getAttribute(attributes, "loc")
                    val vtc = getAttribute(attributes, "vtc")
                    val po = getAttribute(attributes, "po")
                    val dist = getAttribute(attributes, "dist")
                    val subdist = getAttribute(attributes, "subdist")
                    val state = getAttribute(attributes, "state")
                    val pc = getAttribute(attributes, "pc")
                    val dob= getAttribute(attributes, "dob")

                    aadharCard = AadharCard(uid, name, gender, gname, yob, co, house, loc, dist, subdist, lm, vtc, state, street, dob, po, pc)
                }
            }  else {
                val uid = ""
                val name = ""
                val gender = ""
                val yob = ""
                val gname = ""
                val co = ""
                val house = ""
                val street = ""
                val lm = ""
                val loc = ""
                val vtc = ""
                val po = ""
                val dist = ""
                val subdist = ""
                val state = ""
                val pc = ""
                val dob = ""

                aadharCard = AadharCard(uid, name, gender, gname, yob, co, house, loc, dist, subdist, lm, vtc, state, street, dob, po, pc)
            }
            return aadharCard
        }

        //function to get text value using the attribute name
        private fun getAttribute(attributes: NamedNodeMap, attributeName: String): String {
            val node = attributes.getNamedItem(attributeName)
            return if (node != null) {
                node.textContent
            } else {
                ""
            }
        }

        @Throws(ParseException::class)
        private fun formatGender(gender: String): String {
            val lowercaseGender = gender.lowercase()
            return if (lowercaseGender == "male" || lowercaseGender == "m") {
                "M"
            } else if (lowercaseGender == "female" || lowercaseGender == "f") {
                "F"
            } else if (lowercaseGender == "other" || lowercaseGender == "o") {
                "O"
            } else {
                throw ParseException("Gender not found", 0)
            }
        }
    }
}