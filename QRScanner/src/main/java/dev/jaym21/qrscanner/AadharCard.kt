package dev.jaym21.qrscanner

data class AadharCard (
    val uid: String?,
    val name: String?,
    val gender: String?,
    val guardianName: String?,
    val yearOfBirth: String?,
    val careOf: String?,
    val house: String?,
    val location: String?,
    val district: String?,
    val subDistrict: String?,
    val landmark: String?,
    val village: String?,
    val state: String?,
    val street: String?,
    val dateOfBirth: String?,
    val postOffice: String?,
    val pinCode: String?,
)